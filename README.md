# MARS2 Protocol Buffer Definitions

This repository contains the Protocol Buffer definitions for the MARS2 network API. The [Buf](https://docs.buf.build/introduction) tool can be used to pull the .proto file from the respository and manage the code generation. The API network protocol and message format is described in the [Wiki](https://bitbucket.org/uwaploe/mars2api/wiki/Home).

## Useful reference links

+ [Protocol Buffer Guides](https://developers.google.com/protocol-buffers/docs/overview)
+ [Buf Introduction](https://docs.buf.build/introduction)

## Code Generation Example

### Using Buf

You must first install Buf using the [online instructions](https://docs.buf.build/installation). Note that most features of Buf are overkill for this project but the ability to generate code directly from a remote Git repository is very handy.

Create a file named `buf.gen.yaml` in the current directory, this example will generate C++ classes and place the `*.h` and `*.cc` files in a subdirectory named `gen`. You must already have the Protocol Buffer Compiler, `protoc`, installed.

``` yaml
version: v1
plugins:
  - name: cpp
    out: gen
```

Run buf to generate the code:

``` shellsession
$ buf generate https://bitbucket.org/uwaploe/mars2api.git
$ ls ./gen
stream.pb.cc  stream.pb.h
$
```

### Using protoc

Clone this repository on your local system and run the commands below (this assumes you are on a Unix/Linux system and the repo was cloned to the directory `mars2api`).

``` shellsession
$ mkdir gen
$ protoc -I=./mars2api/mars --cpp_out=./gen ./mars2api/mars/stream.proto
$ ls ./gen
stream.pb.cc stream.pb.h
```
